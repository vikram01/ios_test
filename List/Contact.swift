//
//  Contact.swift
//  List
//
//  Created by Vikram Kumar Singh on 6/7/15.
//  Copyright (c) 2015 Vikram Apps. All rights reserved.
//

import Foundation


struct Contact
{
    var id:Int
    var name: String
    var username: String
    var email: String
    var phone: String
    var website: String
    var address: Address
    var company: Company

   
    init()
    {
        id=0
        name=""
        username=""
        email=""
        phone=""
        website=""
        var geoLoc = GeoLocation(lat: "",lng: "")
        address = Address(street: "",suite: "",city: "",zipcode: "", geo: geoLoc)
        company = Company(name: "", catchPhrase: "", bs: "")
        
    }
    
    static func contactWithJSON(results: NSArray) -> [Contact] {
    var contacts = [Contact]()
        println("result count ->\(results.count)")
        if(results.count>0)
        {
            for result in results
            {
                
                let json = JSON(result)
                var contact = Contact()            
                contact.id = json["id"].intValue
                contact.name = json["name"].stringValue
                contact.username = json["username"].stringValue
                contact.email = json["email"].stringValue
                contact.phone = json["phone"].stringValue
                contact.website = json["website"].stringValue
                contact.address.street = json["address"]["street"].stringValue
                contact.address.city = json["address"]["city"].stringValue
                contact.address.suite = json["address"]["suite"].stringValue
                contact.address.zipcode = json["address"]["zipcode"].stringValue
                contact.address.geo.lat = json["address"]["geo"]["lat"].stringValue
                contact.address.geo.lat = json["address"]["geo"]["lng"].stringValue
                contact.company.name = json["company"]["name"].stringValue
                contact.company.catchPhrase = json["company"]["catchPhrase"].stringValue
                contact.company.bs = json["company"]["bs"].stringValue
                contacts.append(contact)
                
            }
        }
        return contacts
    }
}

struct Address {

    var street:String
    var suite: String
    var city: String
    var zipcode: String
    var geo: GeoLocation
}

struct GeoLocation
{
    var lat: String
    var lng : String
    
}

struct Company {

    var name: String
    var catchPhrase: String
    var bs:String
}