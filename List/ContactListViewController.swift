//
//  ViewController.swift
//  List
//
//  Created by Vikram Kumar Singh on 6/7/15.
//  Copyright (c) 2015 Vikram Apps. All rights reserved.
//

import UIKit

class ContactListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, ContactApiProtocol  {

    
    @IBOutlet weak var sortButton: UIBarButtonItem!
    @IBOutlet weak var appsTableView: UITableView!
    var contacts = [Contact]()
    var contactApi : ContactApi!
    let kCellIdentifier: String = "ContactCell"
    var boxView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        contactApi = ContactApi(delegate: self)        
        progressBarDisplayer()
        //activityIndicator.startAnimating()
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        contactApi.getContacts()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(kCellIdentifier) as! UITableViewCell
        
        let contact = self.contacts[indexPath.row]
        cell.detailTextLabel?.text = contact.email
        cell.textLabel?.text = contact.name
        return cell
    }
    
    func contactsReceivedAPIResults(results: NSArray) {
        dispatch_async(dispatch_get_main_queue(), {
            
            self.contacts=Contact.contactWithJSON(results)
            self.appsTableView!.reloadData()
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            //self.activityIndicator.stopAnimating()
            self.boxView.removeFromSuperview()
           
        })
    }
    func handleError(error: String) {
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            //self.activityIndicator.stopAnimating()
            self.boxView.removeFromSuperview()
        
            // show alert
            println("error in getting records \(error)")
            let alertController = UIAlertController(title: "Contact List", message:
            "Error! \(error)", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
        
    }


    func progressBarDisplayer() {
        boxView = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 25, width: 180, height: 50))
        boxView.backgroundColor = UIColor.whiteColor()
        boxView.alpha = 0.8
        boxView.layer.cornerRadius = 10
        
        //Here the activityIndicator is initialized
        var activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        activityView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityView.startAnimating()
        
        var textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
        textLabel.textColor = UIColor.grayColor()
        textLabel.text = "Loading Contacts"
        
        boxView.addSubview(activityView)
        boxView.addSubview(textLabel)
        
        view.addSubview(boxView)    }

    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let detailsViewController: DetailsViewController = segue.destinationViewController as? DetailsViewController {
            var contactIndex = appsTableView!.indexPathForSelectedRow()!.row
            var selectedContact = self.contacts[contactIndex]
            detailsViewController.contact = selectedContact
        }
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.layer.transform = CATransform3DMakeScale(0.1,0.1,1)
        UIView.animateWithDuration(0.25, animations: {
            cell.layer.transform = CATransform3DMakeScale(1,1,1)
        })
    }
    
    @IBAction func sortOnClick(sender: UIBarButtonItem) {
        
        println("here I am\(sender.title)")
        if( contacts.count>0)
        {
            if let sortOrder:String = sender.title as String?
            {
                if(sortOrder == "ASC")
                {
                    contacts.sort(sortForASC)
                    sender.title? = "DESC"
                }
                else
                {
                    contacts.sort(sortForDESC)
                    sender.title? = "ASC"
                }
            }
            appsTableView!.reloadData()
        }
    }
    func sortForASC(one:Contact, two:Contact) -> Bool {
        return one.name < two.name
    }
    
    func sortForDESC(one:Contact, two:Contact) -> Bool {
        return one.name > two.name
    }

}

    




