//
//  ContactApi.swift
//  List
//
//  Created by Vikram Kumar Singh on 6/7/15.
//  Copyright (c) 2015 Vikram Apps. All rights reserved.
//

import Foundation


protocol ContactApiProtocol {
    func contactsReceivedAPIResults(results: NSArray)
    func handleError(error: String)
}


class ContactApi {
    var delegate: ContactApiProtocol
    
    init(delegate: ContactApiProtocol) {
        self.delegate = delegate
    }
    
    func getContacts() {
        let urlPath = "http://jsonplaceholder.typicode.com/users"
        let url = NSURL(string: urlPath)
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithURL(url!, completionHandler: {data, response, error -> Void in
        if(error != nil) {
            // If there is an error in the web request, print it to the console
            println(error.localizedDescription)
            self.delegate.handleError(error.localizedDescription)
        }
        var err: NSError?
        if let jsonResult = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err) as? NSArray{
            if(err != nil) {
                println("JSON Error \(err!.localizedDescription)")
                self.delegate.handleError(err!.localizedDescription)
            }
            else{
                self.delegate.contactsReceivedAPIResults(jsonResult)
            }
        }
                
        })
            
        task.resume()
    }
}