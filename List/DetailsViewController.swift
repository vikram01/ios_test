//
//  DetailsViewController.swift
//  List
//
//  Created by Vikram Kumar Singh on 6/7/15.
//  Copyright (c) 2015 Vikram Apps. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    
    var contact: Contact?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var comanyLabel: UILabel!
    @IBOutlet weak var webLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
   
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = self.contact?.name
        titleLabel.text = self.contact?.username
        phoneLabel.text = self.contact?.phone
        addressLabel.text = self.contact!.address.suite + ", " + self.contact!.address.street  + "\n"
                            +  self.contact!.address.city  + ", " + self.contact!.address.zipcode
        webLabel.text = self.contact?.website
        comanyLabel.text = self.contact!.company.name + "\n" + self.contact!.company.catchPhrase
                           + "\n" + self.contact!.company.bs
        
    }
}